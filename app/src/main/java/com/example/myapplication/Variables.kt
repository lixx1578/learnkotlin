package com.example.myapplication


/**
 * 1) Type is non-null by default.
 * 2) Nullable string: add a "?"
 *      val name: String? = null
 * 3) Type inference
 *      val name = "omit String"
 */

/**
 * 4) Publicly avaiable and Top level variable
 */
var greeting: String? = null

fun main(){
    println("Hello Kotlin")

    /**
     * 5) Local variable: within scope
     * var/val
     */
    val name: String = "Only assign once/read only after assigned"
    var anOtherName : String = "This value can be updated"

    anOtherName = "can change"
    println(greeting)

    /**
     * 6) if statement
     */
    if(greeting != null){
        println(greeting)
    }else{
        println("Hi")
    }

    greeting = "Hello"
    /**
     * 7) when statement
     */
    when(greeting){
        null -> println("Hi")
        else -> println(greeting)
    }

    /**
     * 8) Assigning value to a variable using if statement or when statement
     */
    var greetingToPrint = if(greeting != null) greeting else "Hi"
    greetingToPrint =  when(greeting){
        null -> "Hi"
        else -> greeting
    }
}
