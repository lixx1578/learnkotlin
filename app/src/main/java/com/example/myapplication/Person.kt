package com.example.myapplication

class Person(val firstName : String = "default First Name", val lastName : String = "Default First Name") {

//    val firstName : String = _firstName
//    val lastName : String = _lastName
//
    /**
     * 1) Can initialize parameters in init block
     */
    init {
        println("init 1")
    }

    /**
     * 2) secondary constructor
     */
//    constructor(): this("default First Name", "Default Last Name"){
//        println("secondary constructor")
//    }
//
//    init {
//        println("init 2")
//    }

    var nickName: String? = null
        set(value) {
            /**
             * 3) keyword: field
             * assign value to "field" which field can be used in getter
             */
            field = value
            println("the new nick name is $value")
        }

    get(){
        println("the return value is $field")
        return field
    }

    /**
     * 5) internal: avaiable within module
     *    private: within the same file
     *    protected: within the class or subclass
     */
    internal fun printInfo(){
        /**
         * 4) simplyfy use elvis operator (?:) : val nickNameToPrint = if(nickName !=null) nickName else "no nick name"
         */
        val nickNameToPrint = nickName ?:"no nick name"
        println("$firstName ($nickNameToPrint) $lastName")

        /**
         * 5) Visibility modifier: in kotlin, methods, variable is "Public" by default
         */
    }
}

