package com.example.myapplication

//Top level function
/**
 * 1) Specify return type "String"
 */
fun getGreeting(): String {
    return "Hello Kotlin";
}

/**
 * 2) Function supports type infering too
 *      fun getGreeting() = "Hello Kotlin"
 */
fun getGreetingSame() = "Hello Kotlin"

/**
 * 3)Function parameters
 *
 */

fun sayHello(itemToGreet:String){
    //val msg = "Hello" + itemToGreet

    /**
     * 4)String templates
     */
    val msg = "Hello $itemToGreet"
    println(msg)
}

fun sayHelloSame(itemToGreet: String) = println("Hello $itemToGreet")



fun main(){
    println(getGreeting())
    sayHello("test")
}