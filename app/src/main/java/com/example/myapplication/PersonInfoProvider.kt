package com.example.myapplication

interface PersonInfoProvider {

    /**
     * 3) provide property
     */
    val providerInfo: String

    /**
     * 2) interface can provide basic implementation
     */
    fun printInfo(person: Person){
        println(providerInfo)
        person.printInfo()
    }
}


/**
 * 4) Implement multiple interface
 */
interface SessionInfoProvider{
    fun getSessionId() : String
}

/**
 * 1) ":" : implement
 */
open class BasicInfoProvider : PersonInfoProvider, SessionInfoProvider {

    /**
     * Protected open: avaiable to subclass but not API access
     */
    protected open val sessionIdPrefix = "Session"

    override fun getSessionId(): String {
        return sessionIdPrefix
    }



    override val providerInfo : String
        get() = "BasicInfoProvider"

    override fun printInfo(person: Person) {
        super.printInfo(person)
        println("additional info")

    }

}

fun main(){
    val provider = BasicInfoProvider()
    provider.printInfo(Person())

    checkType(provider)

    val inheritedProvider = FancyInfoProvider()
    inheritedProvider.printInfo(Person())
}

/**
 *  5)Type checking and type casting
 */
fun checkType(infoProvider: PersonInfoProvider){
    if (infoProvider is SessionInfoProvider){
        println("is a session info p")
        /**
         * Because kotlin implemented a smart casting,
         * then you don't need to do this:
         * (infoProvider as SessionInfoProvider).getSessionId()
         */
        infoProvider.getSessionId()

    }else if(infoProvider !is SessionInfoProvider){
        println("not is a session info p")
    }
}