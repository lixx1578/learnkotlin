package com.example.myapplication

fun main(){


    /**
     * 1) Arrays
     */
    val interestingThings = arrayOf("test", "anotherTest", "books")

    /**
     * Collection iteration
     */
    for (it in interestingThings){
        println(it)
    }

    interestingThings.forEach {
        println(it)
    }

    //rename "it"
    interestingThings.forEach {interestingThing ->
        println(interestingThing)
    }

    interestingThings.forEachIndexed{ index, interingThing ->
         println("$interestingThings is at index $index")
    }


    /**
     * 2) List
     */
    val anotherInterestingThings = listOf("test", "anotherTest", "books")


    /**
     * 3) Map
     */
    val map = mapOf(1 to "a", 2 to "b", 3 to "c")
    map.forEach {key, value -> println("$key -> $value") }

    /**
     * 4) By default, collection types are immutable - cannot change values
     */
     val mutableThings = mutableListOf("test", "anotherTest", "books")
     mutableThings.add("somethingNew")

    sayHello("hi");
    sayHello("hi", "test", "anotherTest", "books")

    /**
     * 6) "*": spread operator
     */
    sayHello("hi", *anotherInterestingThings.toTypedArray())

    /**
     * 7) named arguments: pass arguments whatever order we what
     *
     * LIMITATION:
     * However all other arguments need to use named arguments
     * greetPerson(name = "Ou", "hi") -> This is not allowed
     *
     */
    greetPerson(name = "Ou", greeting = "hi")

    /**
     * 8) Default parameter values
     * greetingPerson has default greeting "Hello"
     */

    greetPerson(name = "Ou")

}

/**
 * 5) vararg: variable arguments
 */
fun sayHello(greeting: String, vararg itemsToGreet:String) {
    itemsToGreet.forEach{
        println("$greeting $it")
    }
}

fun greetPerson(greeting: String = "Hello", name: String) = println("$greeting $name")