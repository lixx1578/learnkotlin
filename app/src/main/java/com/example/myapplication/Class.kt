package com.example.myapplication

fun main(){
    val person = Person("Ou", "Li")
    /**
     * Property access syn
     */
    person.lastName
    person.firstName

    val person2 = Person()

    person2.nickName = "JuanJuan"
    println(person2.nickName)

    person.printInfo()
}